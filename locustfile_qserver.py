
import time
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    wait_time = between(5, 10)

    @task
    def test_addtoqueue(self):
        response = self.client.post("/queue",json = {
                        "queue_name": "Hogwards",
                        "priority_status": "True",
                        "key": "Amal",
                        "data": {
                        "name":"Amal",
                        "Customer_ID":"Amal",
                        "mobileno":"9526630462",
                        "account_no": "876543345678",
                        "roomid":"tfkvkg-hcjyujguv-344567",
                        "language":"English",
                        "validated_time":"2020-04-09"
                        }})
        print("Response status code:", response.status_code)
        print("Response content:", response.text)
        time.sleep(1)
    @task
    def test_addtoqueue(self):
        response = self.client.post("/queue",json = {
                        "queue_name": "Hogwards",
                        "priority_status": "True",
                        "key": "Achu",
                        "data": {
                        "name":"Achu",
                        "Customer_ID":"Amal",
                        "mobileno":"9567461638",
                        "account_no": "879876543210",
                        "roomid":"tfkvkg-hcjyujguv-344567",
                        "language":"English",
                        "validated_time":"2020-04-09"
                        }})
        print("Response status code:", response.status_code)
        print("Response content:", response.text)
        time.sleep(1)
    @task
    def test_getqueue(self):
        response = self.client.get("/queue?queue_name=Hogwards")
        print("Response status code:", response.status_code)
        print("Response content:", response.text)
        time.sleep(1)
    @task
    def test_deletefromqueue(self):
        response = self.client.delete("/queue",json = {"queue_name": "Hogwards","key": "Amal"})
        print("Response status code:", response.status_code)
        print("Response content:", response.text)
        time.sleep(1)
    @task
    def test_queuelength(self):
        response = self.client.get("/queue/length?queue_name=Hogwards")
        print("Response status code:", response.status_code)
        print("Response content:", response.text)
        time.sleep(1)
    @task
    def test_queueposition(self):
        response = self.client.get("/position?queue_name=Hogwards&key=Amal")
        print("Response status code:", response.status_code)
        print("Response content:", response.text)
        time.sleep(1)
    @task
    def test_movetoqueue(self):
        response = self.client.post("/position",json = {"from_queue":"Hogwards","to_queue":"Top","key":"Amal"})
        print("Response status code:", response.status_code)
        print("Response content:", response.text)
        time.sleep(1)
    @task
    def test_changequeueposition(self):
        response = self.client.put("/position",json = {"queue_name":"Hogwards","from_key":"Amal","to_key":"Achu"})
        print("Response status code:", response.status_code)
        print("Response content:", response.text)
        time.sleep(1)
    @task
    def test_filteration(self):
        response = self.client.post("/queue/filteration",json = {"queue_name": "Top","lang_list":"English"})
        print("Response status code:", response.status_code)
        print("Response content:", response.text)
        time.sleep(1)
    # @tasks
    # def test_queueclear(self):
    #     response = self.client.delete("/queue/clear")
    #     print("Response status code:", response.status_code)
    #     print("Response content:", response.text)


    def on_start(self):
        pass
    def on_end(self):
        pass
